extern crate cc;


fn main() {
    cc::Build::new()
        .file("../cmpfit-1.2/mpfit.c")
        .compile("libcmpfit");
}