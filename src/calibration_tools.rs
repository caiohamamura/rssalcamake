pub fn fn_range_dependency(r: f64, a: f64, k2: f64, k3: f64) -> f64 {
  ((1.0 / r.powf(a)) * (1.0 - (-k2 * r.powf(k3)).exp()))
}

pub fn fn_effective_reflectance(
  rho: f64,
  ang_phase: f64,
  p0: f64,
  f_r: f64,
  proj_area_ratio: f64,
) -> f64 {
  rho * ang_phase * p0 * f_r * proj_area_ratio
}

pub fn fn_dn(m: f64, rho_eff: f64, c: f64, k: f64) -> f64 {
  (m * rho_eff + c) * (1.0 - (-k * rho_eff).exp())
}

pub fn fn_dn_summarize(
  r: f64,
  rho: f64,
  p0: f64,
  m: f64,
  c: f64,
  k: f64,
  a: f64,
  k2: f64,
  k3: f64,
  ang_phase: f64,
  proj_area_ratio: f64,
) -> f64 {
  let f_r = fn_range_dependency(r, a, k2, k3);
  let rho_eff = fn_effective_reflectance(rho, ang_phase, p0, f_r, proj_area_ratio);
  fn_dn(m, rho_eff, c, k)
}
