extern crate clap;
extern crate csv;

use clap::{App, Arg};
use std::error::Error;
use std::fs::File;
use std::process::exit;

mod params {
    pub const M: usize = 0;
    pub const C: usize = 1;
    pub const K: usize = 2;
    pub const A: usize = 3;
    pub const K2: usize = 4;
    pub const K3: usize = 5;
    pub const P0: usize = 6;
}

mod bindings;
mod calibration_tools;
mod tests;

const DEFAULT_M: f64 = 137.21;
const DEFAULT_C: f64 = 407.04;
const DEFAULT_K: f64 = 16.83;
const DEFAULT_A: f64 = 1.5;
const DEFAULT_K2: f64 = 0.18;
const DEFAULT_K3: f64 = 0.84;
const DEFAULT_P0: f64 = 10.0;

const R_COLUMN: usize = 0;
const RHO_COLUMN: usize = 4;
const DN_COLUMN: usize = 1;

const N_PARAMS: usize = 7;

static mut VERBOSITY:u64 = 0;

#[derive(Debug)]
struct DataHolder {
    r: Vec<f64>,
    rho: Vec<f64>,
    dn: Vec<f64>,
    n: usize,
}

impl DataHolder {
    pub fn new(size: usize) -> DataHolder {
        DataHolder {
            r: Vec::with_capacity(size),
            rho: Vec::with_capacity(size),
            dn: Vec::with_capacity(size),
            n: size,
        }
    }
}

#[no_mangle]
extern "C" fn fn_residuals<'a>(
    m: ::std::os::raw::c_int,
    n: ::std::os::raw::c_int,
    x: *mut f64,
    fvec: *mut f64,
    _dvec: *mut *mut f64,
    private_data: *mut ::std::os::raw::c_void,
) -> std::os::raw::c_int {
    {
        let fvec2 = unsafe { std::slice::from_raw_parts_mut(fvec, m as usize) };
        let inputs = unsafe { &mut *(private_data as *mut DataHolder) };
        let x = unsafe { std::slice::from_raw_parts(x, n as usize) };
        let p0 = x[params::P0];
        let m1 = x[params::M];
        let c = x[params::C];
        let k = x[params::K];
        let a = x[params::A];
        let k2 = x[params::K2];
        let k3 = x[params::K3];
        let angular_phase = 1.0;
        let proj_area_ratio = 1.0;
        let mut newdn = Vec::with_capacity(8);

        /* Compute function deviates */
        let mut i = 0usize;
        let mut residuals_vec:Vec<f64> = Vec::with_capacity(m as usize);
        for element in fvec2.iter_mut() {
            let dn = calibration_tools::fn_dn_summarize(
                inputs.r[i],
                inputs.rho[i],
                p0,
                m1,
                c,
                k,
                a,
                k2,
                k3,
                angular_phase,
                proj_area_ratio,
            );
            newdn.push(dn);
            let residuals = (dn - inputs.dn[i]).powf(2.0f64);
            residuals_vec.push(residuals);
            *element = residuals;
            i += 1;
        }

        let rmse = ((residuals_vec.iter().sum::<f64>())/(m as f64)).sqrt();
        
        match {unsafe{VERBOSITY}} {
            0 => (),
            1 => {
                println!("p0: {}", p0);
                println!("RMSE: {}", rmse);
                println!();
            },
            _ => {
                println!("p0: {}", p0);
                println!("dn_cal: {:?}", newdn);
                println!("dn_real: {:?}", inputs.dn);
                println!("RMSE: {}", rmse);
                println!();
            },
        }

        return 0;
    }
}

fn main() {
    let matches = help();

    let input = matches.value_of("input").unwrap();
    let output_root = matches.value_of("outputRoot").unwrap();
    let fix_p0 = matches.is_present("fixP0");
    let fit_dn = matches.is_present("fitDN");
    let fit_range = matches.is_present("fitRange");
    unsafe {VERBOSITY = matches.occurrences_of("verbose")};
    let m = matches
        .value_of("m")
        .unwrap_or(&DEFAULT_M.to_string())
        .parse()
        .unwrap_or(DEFAULT_M);
    let c = matches
        .value_of("c")
        .unwrap_or(&DEFAULT_C.to_string())
        .parse()
        .unwrap_or(DEFAULT_C);
    let k = matches
        .value_of("k")
        .unwrap_or(&DEFAULT_K.to_string())
        .parse()
        .unwrap_or(DEFAULT_K);
    let a = matches
        .value_of("a")
        .unwrap_or(&DEFAULT_A.to_string())
        .parse()
        .unwrap_or(DEFAULT_A);
    let k2 = matches
        .value_of("k2")
        .unwrap_or(&DEFAULT_K2.to_string())
        .parse()
        .unwrap_or(DEFAULT_K2);
    let k3 = matches
        .value_of("k3")
        .unwrap_or(&DEFAULT_K3.to_string())
        .parse()
        .unwrap_or(DEFAULT_K3);
    let p0 = matches
        .value_of("P0")
        .unwrap_or(&DEFAULT_P0.to_string())
        .parse()
        .unwrap_or(DEFAULT_P0);

    let mut data = read_data(input).unwrap_or_else(|err| {
        eprintln!("{}", err);
        exit(1);
    });

    let mut mp_config = bindings::mp_config {
        ftol: 1e-16,
        xtol: 1e-16,
        gtol: 0.0,
        covtol: 1e-16,
        maxiter: 1000,
        stepfactor: 100.0,
        epsfcn: 0.0,
        nprint: 0,
        douserscale: 0,
        iterproc: std::option::Option::None,
        maxfev: 0,
        nofinitecheck: 0,
    };

    fn get_good_mp_pars() -> bindings::mp_par {
        let mut mp_par = bindings::mp_par::new();
        mp_par.fixed = 0;
        mp_par.side = 2;
        mp_par.relstep = 0.01;
        mp_par
    }
    let mut mp_pars = [get_good_mp_pars(); N_PARAMS];
    mp_pars[params::M].fixed = 1;
    mp_pars[params::C].fixed = 1;
    mp_pars[params::K].fixed = 1;
    mp_pars[params::A].fixed = 1;
    mp_pars[params::K2].fixed = 1;
    mp_pars[params::K3].fixed = 1;
    mp_pars[params::P0].fixed = 0;
    mp_pars[params::P0].limited[0] = 1;
    mp_pars[params::P0].limits[0] = 0.001;
    mp_pars[params::P0].limited[1] = 0;
    mp_pars[params::P0].limits[1] = 0.0;
    if fix_p0 {
        mp_pars[params::P0].fixed = 1;
    }
    if fit_range {
        mp_pars[params::A].fixed = 0;
        mp_pars[params::K2].fixed = 0;
        mp_pars[params::K3].fixed = 0;
    }
    if fit_dn {
        mp_pars[params::M].fixed = 0;
        mp_pars[params::C].fixed = 0;
        mp_pars[params::K].fixed = 0;
    }
    let mut vec_a = vec![0f64; data.n];
    let mut vec_b = vec![0f64; N_PARAMS];
    let mut vec_c = vec![0f64; N_PARAMS * N_PARAMS];
    let mut res =
        bindings::mp_result::new(vec_a.as_mut_ptr(), vec_b.as_mut_ptr(), vec_c.as_mut_ptr());
    let data_ptr: *mut std::os::raw::c_void = &mut data as *mut _ as *mut std::os::raw::c_void;

    let mut p_arr: [f64; 7] = [m, c, k, a, k2, k3, p0];

    let pars_arr = p_arr.as_mut_ptr();
    let pars_conf_arr = mp_pars.as_mut_ptr();

    unsafe {
        bindings::mpfit(
            fn_residuals,
            data.n as i32,
            N_PARAMS as i32,
            pars_arr,
            pars_conf_arr,
            &mut mp_config,
            data_ptr,
            &mut res,
        )
    };

    let f_r = get_fn_range_vec(&data.r, &p_arr);
    let rho_eff = get_rho_vec(f_r, &data.rho, &p_arr);
    let dn_cal = get_dns_vec(&rho_eff, &p_arr);

    // println!("params : {:?}", p_arr[N_PARAMS - 1]);
    // println!("dn_mes : {:?}", data.dn);
    // println!("dn_cal : {:?}", dn_cal);
    // println!("rho_eff: {:?}", rho_eff);
    // println!("rho_cal: {:?}", data.rho);

    write_calibration_data(
        format!("{}.txt", output_root),
        dn_cal,
        data.dn,
        &data.r,
        data.rho,
        rho_eff,
    )
    .unwrap_or_else(|msg| panic!("{}", msg));

    let rmse = get_rmse(res, data.n);

    write_params(format!("{}.par", output_root), &p_arr, rmse)
        .unwrap_or_else(|msg| panic!("{}", msg));

    exit(0);
}

fn get_rmse(res: bindings::mp_result, n: usize) -> f64 {
    let sl = unsafe { std::slice::from_raw_parts(res.resid, n) };
    let rmse: f64 = sl.iter().sum();
    (rmse/(n as f64)).sqrt()
}

use serde::Serialize;

#[derive(Serialize)]
struct CalibrationRow {
    cal_dn_1: f64,
    true_dn_2: f64,
    range_3: f64,
    rho_4: f64,
    rho_eff_5: f64,
}

#[allow(unused_imports)]
use std::io::prelude::*;
use std::io::Write;

fn write_calibration_data(
    output: String,
    dn_cal: Vec<f64>,
    dn_true: Vec<f64>,
    range: &Vec<f64>,
    rho: Vec<f64>,
    rho_eff: Vec<f64>,
) -> Result<(), Box<dyn Error>> {
    let mut file = File::create(output)?;
    file.write_all(b"#1cal_dn 2true_dn 3_range 4_rho 5_rho_eff\n")?;
    let mut wtr = csv::WriterBuilder::new()
        .delimiter(b' ')
        .has_headers(false)
        .from_writer(file);
    for (d1, (d2, (r, (rh, rh_eff)))) in dn_cal
        .iter()
        .zip(dn_true.iter().zip(range.iter().zip(rho.iter().zip(rho_eff.iter()))))
    {
        wtr.serialize(CalibrationRow {
            cal_dn_1: *d1,
            true_dn_2: *d2,
            range_3: *r,
            rho_4: *rh,
            rho_eff_5: *rh_eff,
        })?;
    }
    wtr.flush()?;

    Ok(())
}

fn write_params(output: String, par: &[f64], rmse: f64) -> Result<(), Box<dyn Error>> {
    
    let mut wtr = File::create(output)?;
    wtr.write_all(format!("m {}\n", par[params::M]).as_bytes())?;
    wtr.write_all(format!("c {}\n", par[params::C]).as_bytes())?;
    wtr.write_all(format!("k {}\n", par[params::K]).as_bytes())?;
    wtr.write_all(format!("a {}\n", par[params::A]).as_bytes())?;
    wtr.write_all(format!("k2 {}\n", par[params::K2]).as_bytes())?;
    wtr.write_all(format!("k3 {}\n", par[params::K3]).as_bytes())?;
    wtr.write_all(format!("p0 {}\n", par[params::P0]).as_bytes())?;
    wtr.write_all(format!("RMSE {}\n", rmse).as_bytes())?;

    Ok(())
}

fn get_fn_range_vec(r: &Vec<f64>, pars: &[f64]) -> Vec<f64> {
    r.iter()
        .map(|x| {
            calibration_tools::fn_range_dependency(
                *x,
                pars[params::A],
                pars[params::K2],
                pars[params::K3],
            )
        })
        .collect()
}

fn get_rho_vec(f_r: Vec<f64>, rho: &Vec<f64>, pars: &[f64]) -> Vec<f64> {
    f_r.iter()
        .zip(rho.iter())
        .map(|(range, reflect)| {
            calibration_tools::fn_effective_reflectance(
                *reflect,
                1.0,
                pars[params::P0],
                *range,
                1.0,
            )
        })
        .collect()
}

fn get_dns_vec(rho: &Vec<f64>, pars: &[f64]) -> Vec<f64> {
    rho.iter()
        .map(|r| calibration_tools::fn_dn(pars[params::M], *r, pars[params::C], pars[params::K]))
        .collect()
}

#[allow(bare_trait_objects)]
fn read_data(input: &str) -> Result<DataHolder, Box<Error>> {
    // open file
    let file = File::open(input)?;
    let mut csv_reader = csv::ReaderBuilder::new()
        .delimiter(b' ')
        .has_headers(false)
        .from_reader(file);

    // count lines and rewind
    let n = csv_reader.records().count();
    csv_reader.seek(csv::Position::new())?;

    // populate data into vec
    let mut data = DataHolder::new(n);
    for result in csv_reader.records() {
        let record = result?;
        data.r.push(get_column(&record, R_COLUMN));
        data.rho.push(get_column(&record, RHO_COLUMN));
        data.dn.push(get_column(&record, DN_COLUMN));
    }

    Ok(data)
}

fn get_column(record: &csv::StringRecord, n: usize) -> f64 {
    if let Some(val) = record.get(n) {
        return val.parse().expect("Not a valid number for column");
    }
    panic!("Could not read column {}!", n);
}

fn help() -> clap::ArgMatches<'static> {
    App::new("salcaCal")
        .version("0.1")
        .author(
            "Steven Hancock <steven.hancock@ed.ac.uk>
Caio Hamamura <caiohamamura@gmail.com>",
        )
        .about(
            "Calibrates SALCA using method from:
https://doi.org/10.1109/TGRS.2017.2652140",
        )
        .arg(Arg::with_name("input").value_name("FILE").required(true))
        .arg(
            Arg::with_name("outputRoot")
                .long("output-root")
                .short("o")
                .value_name("root")
                .help("output name root")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("fixP0")
                .long("fix-p0")
                .short("p")
                .help("fix laser power"),
        )
        .arg(
            Arg::with_name("fitDN")
                .long("fit-dn")
                .short("d")
                .help("fit DN parameters"),
        )
        .arg(
            Arg::with_name("fitRange")
                .long("fit-range")
                .short("r")
                .help("fit range parameters"),
        )
        .arg(
            Arg::with_name("P0")
                .long("p0")
                .value_name("P0")
                .takes_value(true)
                .help("sets P0 multiplication factor for fitting constant for equation 3 (DN fitting)"),
        )
        .arg(
            Arg::with_name("m")
                .long("m")
                .value_name("m")
                .takes_value(true)
                .help("sets m multiplication factor for fitting constant for equation 3 (DN fitting)"),
        )
        .arg(
            Arg::with_name("c")
                .long("c")
                .value_name("c")
                .takes_value(true)
                .help("sets c fitting constant for equation 3"),
        )
        .arg(
            Arg::with_name("k")
                .long("k")
                .value_name("k")
                .takes_value(true)
                .help("sets k fitting constant for equation 3"),
        )
        .arg(
            Arg::with_name("a")
                .long("a")
                .value_name("a")
                .takes_value(true)
                .help("sets fix a fitting constant for equation 5"),
        )
        .arg(
            Arg::with_name("k2")
                .long("k2")
                .value_name("k2")
                .takes_value(true)
                .help("sets k2 fitting constant for equation 5"),
        )
        .arg(
            Arg::with_name("k3")
                .long("k3")
                .value_name("k3")
                .takes_value(true)
                .help("sets k3 fitting constant for equation 5"),
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches()
}
